
#splitting up into training set and testing set: by trainIdx
trainIdx = sample.split(y, SplitRatio = 2/3, group = NULL)

trainData = x[trainIdx, ]
trainClass = y[trainIdx]
testData = x[!trainIdx, ]
testClass = y[!trainIdx]

#svm classifications
svm(trainData, trainClass, type = "C-classification") -> model

#make predictions based on training data and testing data
predict(model, trainData) -> predTrain
predict(model, testData) -> predTest

#create confusion matrices based on predictions
tabTrain = table(predTrain, trainClass)
tabTest = table(predTest, testClass)

#calculate specificity/sensitivity of each cancer type: 
#specificity is value/row sum
#sensitivity is val/col sum
per_cancer_sensitivity = array(NA);
per_cancer_specificity = array(NA);
for(i in 1:nrow(tabTest)){
    per_cancer_sensitivity[i] = tabTest[i, i]/(sum(tabTest[, i]))
    per_cancer_specificity[i] = tabTest[i, i]/(sum(tabTest[i ,]))
}

cbind(rownames(tabTrain), per_cancer_sensitivity) -> per_cancer_sensitivity
cbind(rownames(tabTrain), per_cancer_specificity) -> per_cancer_specificity

#redo svm classification with only two classes (cancer/non cancerous)
y = malignancy_classification[2:length(malignancy_classification)]

trainData = x[trainIdx, ]
trainClass = y[trainIdx]
testData = x[!trainIdx, ]
testClass = y[!trainIdx]

#svm classifation
svm(trainData, trainClass, type = "C-classification") -> model

#make predictions and confusion matrices
predict(model, testData) -> predCancerOrNoCancer
confusionMat = table(predCancerOrNoCancer, testClass)

#calculate statistics to show effectiveness of classification
accuracy = classAgreement(tabTest)$diag
sensitivity = confusionMat["P", "P"]/(sum(confusionMat[, "P"]))
specificity = confusionMat["N", "N"]/(sum(confusionMat[, "N"]))
ppv = confusionMat["P", "P"]/(sum(confusionMat["P", ]))
npv = confusionMat["N", "N"]/(sum(confusionMat["N", ]))
