
#Cancer Datasets to analyze
#had to remove COADREAD, STES, and ESCA because had duplicate patients: thought it would affect SVM classification
cancer = c("ACC", "CESC", "CHOL", "DLBC", "ESCA", "FPPP-FFPE", "HNSC", "KICH", "KIRP", "LGG", "LIHC", "LUSC", "MESO", "OV", "PAAD", "PCPG", "SKCM", "STAD", "TGCT", "THYM", "UCS")

#keep track of mirna data, cancer classification data, number of samples of each type per dataset, and 
#whether data is benign or malignant
mirna_classification <- array(NA)
normal_num <- cancer_num <- num_per_dataset <- array(NA)
malignancy_classification <- array(NA)

for(x in cancer) {
  
  #read from directory with cancer name
  name = paste("gdac.broadinstitute.org_",".miRseq_Mature_Preprocess.Level_3.2015040200.0.0/",".miRseq_mature_RPM_log2.txt", sep=x)
  
  #read in mirna data
  data_t =  read.table(name, header = TRUE)
  cols = colnames(data_t)
  normalc = 0; cancerc = 0;
  
  #assign classification data based on name of sample
  #01, 02 is cancerous, 11 is normal
  for(p in 2:ncol(data_t)){
    split_list = unlist(strsplit(cols[p], '[.]'))
    
    if(substr(split_list[4], 1, 1) != "0"){
      mirna_classification <- c(mirna_classification, "#NONC")
      malignancy_classification <- c(malignancy_classification, "N")
      normalc = normalc + 1
    }
    else{
      mirna_classification <- c(mirna_classification, x)
      malignancy_classification <- c(malignancy_classification, "P")
      cancerc = cancerc + 1
    }
    normal_num[x] = normalc;
    cancer_num[x] = cancerc;
    num_per_dataset[x] = normalc + cancerc;
  }
  #attach data to larger dataset
  if(x != "ACC") {
    data_t = data_t[, 2:ncol(data_t)]
    mirna <- cbind(mirna, data_t)
  }
  else {
    mirna <- data_t
  }
}


